# Script to check the connection to the database we created earlier airflowdb
from datetime import timedelta
# The DAG object; we'll need this to instantiate a DAG
from airflow import DAG
# Operators; we need this to operate!
from airflow.operators.bash_operator import BashOperator
from airflow.utils.dates import days_ago
# These args will get passed on to each operator
# You can override them on a per-task basis during operator initialization
default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': days_ago(2),
    'email': ['airflow@example.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    #'retries': 1,
    #'retry_delay': timedelta(minutes=5),
    # 'queue': 'bash_queue',
    # 'pool': 'backfill',
    # 'priority_weight': 10,
    # 'end_date': datetime(2016, 1, 1),
    # 'wait_for_downstream': False,
    # 'dag': dag,
    # 'sla': timedelta(hours=2),
    # 'execution_timeout': timedelta(seconds=300),
    # 'on_failure_callback': some_function,
    # 'on_success_callback': some_other_function,
    # 'on_retry_callback': another_function,
    # 'sla_miss_callback': yet_another_function,
    # 'trigger_rule': 'all_success'
}
dag = DAG(
    'create_table',
    default_args=default_args,
    description='A simple create table DAG',
    schedule_interval=timedelta(days=1),
)

# importing the connector from mysqlclient
import MySQLdb


def create_table():
    """Create new table in a my_database
   
    Args:
        my_database (dict): details for the db
        new_table (str): name of the table to create
    """

    dbconnect = MySQLdb.connect(host="10.245.166.6",
                              user="de_airflow",
                              passwd="d3_@irfloW",
                              db="de_airflow")

    # create a cursor for the queries
    cursor = dbconnect.cursor()
    cursor.execute("USE de_airflow")

    # here we delete the table, it can be kept or else
    cursor.execute("DROP TABLE IF EXISTS USER_PROFILE")

    # these matches the Twitter data
    query = (
        "CREATE TABLE `USER_PROFILE` ("
        "  `id` INT(11) NOT NULL AUTO_INCREMENT,"
        "  `user` varchar(100) NOT NULL ,"
        "  `created_at` timestamp,"
        "  `id_str` varchar(100),"
        "  PRIMARY KEY (`id`))"
    )

    cursor.execute(query)
    cursor.close()
    dbconnect.close()

from airflow.operators.python_operator import PythonOperator
create_table_dag = PythonOperator(
    task_id='create_table',
    python_callable=create_table,
	dag=dag)
	
# Define the order in which the tasks complete by using the >> and <<
create_table_dag
